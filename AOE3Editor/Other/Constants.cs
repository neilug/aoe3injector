﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOE3Editor.Other
{
    public static class Constants
    {
        public const string DataPath = "./Data/";
        public const string ProtoPath = "./Proto/";
        public const string TechtreePath = "./Techtree/";
        public const string NewFolder = "New/";
        public const string UpdateFolder = "Update/";

        public const string ProtoyName = "protoy.xml";
        public const string TechtreeyName = "techtreey.xml";

        public static string NewProtoPath = $"{ProtoPath}{NewFolder}";
        public static string UpdateProtoPath = $"{ProtoPath}{UpdateFolder}";
        public static string NewTechtreePath = $"{TechtreePath}{NewFolder}";
        public static string UpdateTechtreePath = $"{TechtreePath}{UpdateFolder}";
        public static string DataProtoyXml = $"{DataPath}{ProtoyName}";
        public static string DataTechtreeyXml = $"{DataPath}{TechtreeyName}";

        public const string Welcome = "Welcome ! ";
        public const string WhichOfThoseActionsDoYouWishToDo = "Which of those actions do you wish to do ? :";
        public const string ChooseANumberAndPressEnterToValidate = "Choose a Number and press Enter to validate";
        public const string FirstChoice = "1 - Add and Update Protoy";
        public const string SecondChoice = "2 - Add and Update Techtreey";
        public const string ThirdChoice = "3 - Add and Update All";
        public const string ExitChoice = "4 - Exit";
        public const string ErrorChoiceNotUnderstood = "Sorry, but this choice is not avaiable";

        public const string New = "New :";
        public const string Update = "Udpate :";

        public const string ErrorProtoyNotFound = "Sorry, the file 'protoy.xml' hasn't been found. Are you sure it is in ~/Data/ ?";
        public const string ErrorTechtreeyNotFound = "Sorry, the file 'techtree.xml' hasn't been found. Are you sure it is in ~/Data/ ?";
        public const string ErrorNoNewOrUpdatedProto = "No New or Update folder in ~/Proto/";
        public const string ErrorNoNewOrUpdatedTechtree = "No New or Update folder in ~/Techtree/";
        public const string ElementWithThatNameAlreadyExistsIn = "Element with similar name already exist in ~/Data/";
        public const string ListIsEmpty = "No files found";

        public const string CantGetLastId = "Can't get Last ID in the document. Are you sure the document is properly set in Data/ ?";
        public const string ElementNotFoundInOriginalData = "Element not found in the original data. Are you sure it is an Update folder ?";


        public const string Valid = "o";
        public const string NotValid = "x";

        public const string DatabaseIdentifier = "dbid";
        public const string DatabaseName = "name";
    }
}
