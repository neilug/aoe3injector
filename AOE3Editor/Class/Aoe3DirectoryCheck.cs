using System;
using System.Reflection;
using System.Linq;
using AOE3Editor.Other;
using System.IO;

namespace AOE3Editor.Class
{
    /// <summary>
    /// Running the constructor will informs user if Data has been set properly.
    /// </summary>
    public class Aoe3DirectoryCheck
    {
        public readonly bool HasNewProtoyResources;
        public readonly bool HasUpdateProtoyResources;
        public readonly bool HasProtoyResources;
        public readonly bool HasNewTechtreeyResources;
        public readonly bool HasUpdateTechtreeyResources;
        public readonly bool HasTechtreeyResources;

        public readonly bool HasProtoyXml;
        public readonly bool HasTechtreeyXml;

        public Aoe3DirectoryCheck()
        {
            HasNewProtoyResources = ContainsDirectory(Constants.NewProtoPath);
            HasUpdateProtoyResources = ContainsDirectory(Constants.UpdateProtoPath);
            HasNewTechtreeyResources = ContainsDirectory(Constants.NewTechtreePath);
            HasUpdateTechtreeyResources = ContainsDirectory(Constants.UpdateTechtreePath);

            HasProtoyResources = HasNewProtoyResources || HasUpdateProtoyResources;
            HasTechtreeyResources = HasNewTechtreeyResources || HasUpdateTechtreeyResources;

            HasProtoyXml = ContainsFile(Constants.DataProtoyXml);
            HasTechtreeyXml = ContainsFile(Constants.DataTechtreeyXml);
        }

        public bool ContainsDirectory(string path)
        {
            return Directory.Exists(path);
        }

        public bool ContainsFile(string path)
        {
            return File.Exists(path);
        }
    }
}