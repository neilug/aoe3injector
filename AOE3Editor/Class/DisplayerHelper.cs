﻿using AOE3Editor.Other;
using System;
using System.Linq;
using System.Text;

namespace AOE3Editor.Class
{
    public static class DisplayerHelper
    {
        public static void ApplicationName()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write(string.Concat(Enumerable.Repeat(" ", 12)));
            Console.WriteLine(string.Concat(Enumerable.Repeat("*", nameof(AOE3Editor).Length + 4)));
            Console.Write(string.Concat(Enumerable.Repeat(" ", 12)));
            Console.Write("* ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(nameof(AOE3Editor));
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(" *");
            Console.Write(string.Concat(Enumerable.Repeat(" ", 12)));
            Console.WriteLine(string.Concat(Enumerable.Repeat("*", nameof(AOE3Editor).Length + 4)));
            Console.ResetColor();
        }

        public static void ErrorAndNewLine(string errorProtoyNotFound)
        {
            InRed(errorProtoyNotFound);
            Console.WriteLine();
        }

        public static void Dialog(string text)
        {
            Console.WriteLine($"  {text}");
        }

        public static void InRed(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write(text);
            Console.ResetColor();
        }

        public static void InGreen(string text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(text);
            Console.ResetColor();
        }

        public static void NewLine()
        {
            Console.WriteLine();
        }

        public static void Reset()
        {
            Console.Clear();
            NewLine();
        }

        public static string WaitForUser()
        {
            Console.WriteLine();
            Console.Write(" $>");
            return Console.ReadLine();
        }

        public static void TestValid(string filename)
        {
            Console.Write("  ");
            InGreen(Constants.Valid);
            Console.WriteLine($" - {filename}");
        }

        public static void TestFail(string filename)
        {
            Console.Write("  ");
            InRed(Constants.NotValid);
            Console.Write($" - {filename}");
        }

        public static void ErrorMessage(string message)
        {
            Console.Write(" : ");
            InRed(message);
            Console.WriteLine();
        }
    }
}