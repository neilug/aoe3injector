﻿using aoe3editor.Interface;
using AOE3Editor.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AOE3Editor.Class
{
    public class Injector : IInjector
    {
        private enum ReturnType
        {
            OK = 0,
            CantGetLastId,
            ElementNotFoundInOriginalData,
            ElementAlreadyExistsWithSimilarName,
            ListIsEmpty
        }

        private XmlDocument _protoyDocument;
        private XmlDocument _techtreeyDocument;

        public Injector()
        {
            _protoyDocument = NewXmlDocumentFromDirectoryAndSaveStream(Constants.DataPath, Constants.ProtoyName);
            _techtreeyDocument = NewXmlDocumentFromDirectoryAndSaveStream(Constants.DataPath, Constants.TechtreeyName);
        }

        private XmlDocument NewXmlDocumentFromDirectoryAndSaveStream(string path, string filename)
        {
            var document = new XmlDocument();
            //document.PreserveWhitespace = true;
            try
            {
                document.Load(path + filename);
            }
            catch (XmlException)
            {
                throw;
            }
            return document;
        }

        private string GetNewIDFromDocument(XmlDocument rootDocument)
        {
            int value;

            int.TryParse(rootDocument.DocumentElement.LastChild.FirstChild.FirstChild.Value, out value);
            value++;
            return value.ToString();
        }

        // i'm aware it's not very opti but well ¯\_(ツ)_/¯
        private XmlElement GetElementInDocument(XmlDocument xmlDocument, XmlNode element)
        {
            try
            {
                foreach (XmlElement child in xmlDocument.FirstChild.ChildNodes)
                {
                    if (child.Attributes[Constants.DatabaseName].Value == element.Attributes[Constants.DatabaseName].Value)
                        return child;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        private ReturnType AddNewElement(XmlDocument protoyOrTechtreey, XmlNode newElement, string savingFilename)
        {
            var rootDocument = protoyOrTechtreey;

            var newID = GetNewIDFromDocument(rootDocument);

            if (GetElementInDocument(rootDocument, newElement) != null)
                return ReturnType.ElementAlreadyExistsWithSimilarName;

            if (string.Equals(newID, 0.ToString()))
                return ReturnType.CantGetLastId;

            newElement.FirstChild.FirstChild.Value = newID;

            var importedNode = rootDocument.ImportNode(newElement, true);

            rootDocument.LastChild.AppendChild(importedNode);

            rootDocument.Save(savingFilename);

            return ReturnType.OK;
        }

        private ReturnType UpdateElement(XmlDocument protoyOrTechtreey, XmlNode updatedElement, string savingFilename)
        {
            var rootDocument = protoyOrTechtreey;

            var element = GetElementInDocument(rootDocument, updatedElement);
            if (element == null)
                return ReturnType.ElementNotFoundInOriginalData;

            updatedElement = rootDocument.ImportNode(updatedElement, true);
            rootDocument.FirstChild.ReplaceChild(updatedElement, element);

            rootDocument.Save(savingFilename);

            return ReturnType.OK;
        }

        private List<XmlNode> GetListOfElement(string pathToFileOrFolder)
        {
            List<XmlNode> list = new List<XmlNode>();

            foreach (var name in Directory.GetFiles(pathToFileOrFolder))
            {
                var stream = new StreamReader(name);
                var newDoc = new XmlDocument();
                newDoc.Load(stream);
                list.Add(newDoc.DocumentElement);
                stream.Close();
            }

            return list;
        }

        private void ReadAndAddXml(string path, XmlDocument document, string savingFilename)
        {
            var listOfNewUnit = GetListOfElement(path);

            DisplayerHelper.Dialog(Constants.New);

            if (listOfNewUnit.Count == 0)
            {
                DisplayerHelper.TestFail(path);
                DisplayerHelper.ErrorMessage(Constants.ListIsEmpty);
                return;
            }

            foreach (var unit in listOfNewUnit)
            {
                var result = AddNewElement(document, unit, savingFilename);

                if (result == ReturnType.OK)
                    DisplayerHelper.TestValid(unit.Attributes["name"].Value);
                else if (result == ReturnType.CantGetLastId)
                {
                    DisplayerHelper.TestFail(unit.Attributes["name"].Value);
                    DisplayerHelper.ErrorMessage(Constants.CantGetLastId);
                }
                else if (result == ReturnType.ElementAlreadyExistsWithSimilarName)
                {
                    DisplayerHelper.TestFail(unit.Attributes["name"].Value);
                    DisplayerHelper.ErrorMessage(Constants.ElementWithThatNameAlreadyExistsIn);
                }
            }
        }

        private void ReadAndUpdateXml(string path, XmlDocument document, string savingFilename)
        {
            var listOfUpdateUnit = GetListOfElement(path);

            DisplayerHelper.Dialog(Constants.Update);

            if (listOfUpdateUnit.Count == 0)
            {
                DisplayerHelper.TestFail(path);
                DisplayerHelper.ErrorMessage(Constants.ListIsEmpty);
                return;
            }

            foreach (var unit in listOfUpdateUnit)
            {
                var result = UpdateElement(document, unit, savingFilename);

                if (result == ReturnType.OK)
                    DisplayerHelper.TestValid(unit.Attributes[Constants.DatabaseName].Value);
                else if (result == ReturnType.CantGetLastId)
                {
                    DisplayerHelper.TestFail(unit.Attributes[Constants.DatabaseName].Value);
                    DisplayerHelper.ErrorMessage(Constants.CantGetLastId);
                }
                else if (result == ReturnType.ElementNotFoundInOriginalData)
                {
                    DisplayerHelper.TestFail(unit.Attributes[Constants.DatabaseName].Value);
                    DisplayerHelper.ErrorMessage(Constants.ElementNotFoundInOriginalData);
                }
                else if (result == ReturnType.ElementAlreadyExistsWithSimilarName)
                {
                    DisplayerHelper.TestFail(unit.Attributes[Constants.DatabaseName].Value);
                    DisplayerHelper.ErrorMessage($"{Constants.ElementWithThatNameAlreadyExistsIn}{savingFilename}");
                }
            }
        }

        public void AddAllNewUnit()
        {
            try
            {
                ReadAndAddXml(Constants.NewProtoPath, _protoyDocument, Constants.DataProtoyXml);
            }
            catch (Exception exception)
            {
                DisplayerHelper.NewLine();
                DisplayerHelper.ErrorAndNewLine(exception.Message);
            }
        }

        public void UpdateAllUnit()
        {
            try
            { 
                ReadAndUpdateXml(Constants.UpdateProtoPath, _protoyDocument, Constants.DataProtoyXml);
            }
            catch (Exception exception)
            {
                DisplayerHelper.NewLine();
                DisplayerHelper.ErrorAndNewLine(exception.Message);
            }
    }

        public void AddAllNewTechtree()
        {
            try
            {
                ReadAndAddXml(Constants.NewTechtreePath, _techtreeyDocument, Constants.DataTechtreeyXml);
            }
            catch (Exception exception)
            {
                DisplayerHelper.NewLine();
                DisplayerHelper.ErrorAndNewLine(exception.Message);
            }
        }

        public void UpdateAllTechtree()
        {
            try
            {
                ReadAndUpdateXml(Constants.UpdateTechtreePath, _techtreeyDocument, Constants.DataTechtreeyXml);
            }
            catch (Exception exception)
            {
                DisplayerHelper.NewLine();
                DisplayerHelper.ErrorAndNewLine(exception.Message);
            }
        }
    }
}
