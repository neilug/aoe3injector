﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aoe3editor.Interface
{
    interface IInjector
    {
        /* protoy */
        void AddAllNewUnit();
        void UpdateAllUnit();

        /* techtree */
        void AddAllNewTechtree();
        void UpdateAllTechtree();
    }
}
