﻿using System;
using System.Reflection;
using AOE3Editor.Class;
using AOE3Editor.Other;

namespace AOE3Editor
{
    class Program
    {

        static void Main(string[] args)
        {
            DisplayerHelper.ApplicationName();

            var checker = new Aoe3DirectoryCheck();

            if (!checker.HasProtoyXml)
            {
                DisplayerHelper.ErrorAndNewLine(Constants.ErrorProtoyNotFound);
                DisplayerHelper.WaitForUser();
                return;
            }

            if (!checker.HasTechtreeyXml)
            {
                DisplayerHelper.ErrorAndNewLine(Constants.ErrorTechtreeyNotFound);
                DisplayerHelper.WaitForUser();
                return;
            }
            DisplayerHelper.NewLine();
            DisplayerHelper.Dialog(Constants.Welcome);
            MainLoop();

        }

        public static void MainLoop()
        {
            var checker = new Aoe3DirectoryCheck();
            var injector = new Injector();

            DisplayerHelper.NewLine();
            DisplayerHelper.Dialog(Constants.WhichOfThoseActionsDoYouWishToDo);
            DisplayerHelper.Dialog(Constants.FirstChoice);
            DisplayerHelper.Dialog(Constants.SecondChoice);
            DisplayerHelper.Dialog(Constants.ThirdChoice);
            DisplayerHelper.Dialog(Constants.ExitChoice);
            DisplayerHelper.NewLine();

            string userAnswer;
            while (!string.Equals(userAnswer = DisplayerHelper.WaitForUser(), 4.ToString()))
            {
                // we are in loop until player exit
                if (string.Equals(userAnswer, 1.ToString()))
                {
                    // We write in proto
                    if (!checker.HasProtoyResources)
                    {
                        MainLoop();
                    }
                    else
                    {
                        injector.AddAllNewUnit();
                        DisplayerHelper.NewLine();
                        injector.UpdateAllUnit();
                        DisplayerHelper.NewLine();
                        MainLoop();
                        return;
                    }
                }
                else if (string.Equals(userAnswer, 2.ToString()))
                {
                    // we write in techtree
                    if (!checker.HasTechtreeyResources)
                    {
                        MainLoop();
                    }
                    else
                    {
                        injector.AddAllNewTechtree();
                        DisplayerHelper.NewLine();
                        injector.UpdateAllTechtree();
                        DisplayerHelper.NewLine();
                        MainLoop();
                        return;
                    }
                }
                else if (string.Equals(userAnswer, 3.ToString()))
                {
                    if (!checker.HasTechtreeyResources ||
                        !checker.HasProtoyResources)
                    {
                        MainLoop();
                    }
                    else
                    {
                        DisplayerHelper.NewLine();
                        Console.WriteLine($" {Constants.DataTechtreeyXml} :");
                        DisplayerHelper.NewLine();
                        injector.AddAllNewTechtree();
                        DisplayerHelper.NewLine();
                        injector.UpdateAllTechtree();
                        DisplayerHelper.NewLine();
                        Console.WriteLine($" {Constants.DataProtoyXml} :");
                        DisplayerHelper.NewLine();
                        injector.AddAllNewUnit();
                        DisplayerHelper.NewLine();
                        injector.UpdateAllUnit();
                        DisplayerHelper.NewLine();
                        MainLoop();
                        return;
                    }
                    return;
                }
                else
                {
                    DisplayerHelper.Reset();
                    DisplayerHelper.ErrorAndNewLine(Constants.ErrorChoiceNotUnderstood);
                    DisplayerHelper.Dialog(Constants.WhichOfThoseActionsDoYouWishToDo);
                    DisplayerHelper.Dialog(Constants.FirstChoice);
                    DisplayerHelper.Dialog(Constants.SecondChoice);
                    DisplayerHelper.Dialog(Constants.ThirdChoice);
                    DisplayerHelper.Dialog(Constants.ExitChoice);
                }
            }
        }
    }
}
