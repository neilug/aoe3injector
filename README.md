# AOE3Editor

## How to Setup

You need to place the AOE3Editor.exe in an empty directory, along with :

- a **Data** Folder
- a **Techtree** Folder
- a **Protoy** Folder

In Data :

- protoy.xml (original)
- techtreey.xml (original)

In Techtree :

- a **New** folder, where you will put all your new xml Elements from _techtreey.xml_
- a **Update** folder, where you will put all your updated xml Elements from _techtreey.xml_

In Protoy :

- a **New** folder, where you will put all your new xml Elements from _techtreey.xml_
- a **Update** folder, where you will put all your updated xml Elements _from techtreey.xml_

## Get Started

Everything you need is in Build ! :)

![alt text](Images/tuto1.png)